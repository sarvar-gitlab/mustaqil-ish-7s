<?php

namespace App\Enums;

enum OrderStatus:string {

    case ORDERED = 'ordered';

    case ACCEPTED_BY_CHEF = 'acceptedByChef';

    case READY_TO_DELIVERY = 'readyToDelivery';

    case ACCEPTED_BY_CLIENT = 'acceptedByClient';

    case PAID = 'paid';

    case REJECTED = 'rejected';

}