<?php

namespace App\Enums;

enum ProductType:string {

    case FIRST_COURSE = 'first_course';

    case MAIN_COURSE = 'main_course';

    case APPETIZER = 'appetizer';

    case BEVERAGE = 'beverage';

    case SALAD = 'salad';
    
    case DESSERT = 'dessert';

    case SPECIAL = 'special';

}