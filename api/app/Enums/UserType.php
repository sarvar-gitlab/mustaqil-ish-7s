<?php

namespace App\Enums;

enum UserType: string {
    case WAITER = 'waiter';
    
    case MANAGER = 'manager';
    
    case CASHIER = 'cashier';
    
    case CHEF = 'chef';
    
}