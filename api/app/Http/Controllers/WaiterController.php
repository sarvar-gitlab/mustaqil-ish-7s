<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Table;

class WaiterController extends Controller
{
    public function products(Request $request)
    {
        $query = Product::query();

        if ($request->has('search')) {
            $query->where('name', 'like', '%' . $request->input('search') . '%');
        }

        $products = $query->paginate(4); 

        return response()->json([
            'products' => $products,
        ]);
    }

    public function tables()
    {
        $tables = Table::All();

        return response()->json([
            'tables' => $tables,
        ]);
    }

    public function order(Request $request)
    {
        try {
            $requestData = $request->json()->all();

            $tableId = $requestData['table_id'];
            $waiterId = $requestData['waiter_id'];
            $orderDetails = $requestData['order_details'];

            for ($i = 0; $i < sizeof($orderDetails); $i++) {
                $quantity = $orderDetails[$i]['quantity'];
                $productDataForInsert = Product::find($orderDetails[$i]['product_id']);
                $productPriceForInsert = $productDataForInsert->price * $quantity;

                $orderDataForInsert = [
                    'waiter_id' => $waiterId,
                    'status' => 'ordered',
                    'price' => $productPriceForInsert,
                    'table_id' => $tableId,
                ];

                $order = Order::create($orderDataForInsert);

                $orderId = $order->id;
                $orderItemDataForInsert = [
                    'order_id' => $orderId,
                    'product_id' => $orderDetails[$i]['product_id'],
                    'price' => $productPriceForInsert,
                    'additional' => $orderDetails[$i]['additional'] ?? null,
                    'quantity' => $quantity,
                ];

                OrderItem::create($orderItemDataForInsert);
            }

            return response()->json(['status' => 'success',]);

        } catch (QueryException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Error processing the order. Please try again later.',
            ], 500);
        }
    }

    public function myorders(Request $request)
    {
        $user = auth()->user();

        $myOrders = Order::with(['chef'])
                        ->where('waiter_id', $user->id)
                        ->orderByDesc('updated_at')
                        ->get();

        $formattedOrders = $myOrders->map(function ($order) {
            return [
                'id' => $order->id,
                'chef_name' => optional($order->chef)->name, 
                'status' => $order->status,
                'price' => $order->price,
                'table_id' => $order->table_id,
                'created_at' => $order->created_at,
                'updated_at' => $order->updated_at,
            ];
        });

        return response()->json([
            'status' => 'success',
            'myorders' => $formattedOrders,
        ], 200);
    }

}
