<?php

namespace App\Models;

use App\Enums\TableStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'status' => TableStatus::class,
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    // You can define relationships here if needed
    // For example, if there's a relationship with the orders table

    /**
     * Get the orders for the table.
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getCurrentOrders(){
        return $this->hasMany(Order::class)->where('status', 'acceptedByClient');
    }
}
