<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('chef_id')->nullable();
            $table->unsignedBigInteger('waiter_id')->nullable();
            $table->enum('status', ['ordered', 'acceptedByChef', 'readyToDelivery', 'acceptedByClient', 'paid', 'rejected']);
            $table->unsignedBigInteger('price');
            $table->unsignedBigInteger('table_id');
            $table->timestamps();

            $table->foreign('chef_id')->references('id')->on('users');
            $table->foreign('waiter_id')->references('id')->on('users');
            $table->foreign('table_id')->references('id')->on('tables');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
