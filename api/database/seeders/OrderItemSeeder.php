<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Product;

class OrderItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Assuming you have the Order and Product models

        $orders = Order::all();
        $products = Product::all();

        $minValue = 10000;
        $maxValue = 150000;
        $step = 10000;

        $randomNumbers = [];

        for ($i = $minValue; $i <= $maxValue; $i += $step) {
            $randomNumbers[] = $i;
        }

        foreach ($orders as $order) {
            $productId = $products->random()->id;
            $randomIndex = array_rand($randomNumbers);
            $randomValue = $randomNumbers[$randomIndex];

            OrderItem::create([
                'order_id' => $order->id,
                'product_id' => $productId,
                'price' => $randomValue, 
                'quantity' => rand(1, 10), 
                'additional' => 'Extra cheese', 
            ]);
        }
    }
}
