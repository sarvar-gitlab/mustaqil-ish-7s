<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\User;
use App\Models\Table;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $minValue = 10000;
        $maxValue = 150000;
        $step = 10000;

        $randomNumbers = [];

        for ($i = $minValue; $i <= $maxValue; $i += $step) {
            $randomNumbers[] = $i;
        }
        
        for ($i = 0; $i < 100; $i++) {
            $waiterId = User::where('type', 'waiter')->inRandomOrder()->first()->id;
            $chefId = User::where('type','chef')->inRandomOrder()->first()->id;
            $tableId = Table::inRandomOrder()->first()->id;
            $status = array('ordered', 'acceptedByChef', 'readyToDelivery', 'acceptedByClient', 'paid', 'rejected');
            $rand_status = $status[array_rand($status)];
            $createdAt = now()->subDays(rand(1, 30))->subHours(rand(1, 24))->subMinutes(rand(1, 60));

            $randomIndex = array_rand($randomNumbers);
            $randomValue = $randomNumbers[$randomIndex];

            Order::create([
                'chef_id' => $chefId,
                'waiter_id' => $waiterId,
                'status' => $rand_status,
                'price' => $randomValue, 
                'table_id' => $tableId,
                'created_at' => $createdAt,
            ]);
        }
    }
}

