<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $types = ['appetizer', 'first_course', 'main_course', 'beverage', 'salad', 'dessert', 'special'];
        $productNames = ["Sho'rva", "Manti", "Osh", "Soch kabob", "Go'shsay", "Lo'la kabob", "Xarcho", "Jiz", "Lag'mon" ];

        $minValue = 10000;
        $maxValue = 150000;
        $step = 10000;

        $randomNumbers = [];

        for ($i = $minValue; $i <= $maxValue; $i += $step) {
            $randomNumbers[] = $i;
        }

        for ($i = 1; $i < 10; $i++) {
            $randomIndex = array_rand($randomNumbers);
            $randomValue = $randomNumbers[$randomIndex];
            
            $description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut';
            
            Product::create([
                'photo' => 'product_photos/f'.($i).'.png', 
                'name' => $productNames[$i-1],
                'description' => $description, 
                'is_available' => rand(0, 1), 
                'price' => $randomValue, 
                'type' => $types[array_rand($types)],
            ]);
        }
    }
}
