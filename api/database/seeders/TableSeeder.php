<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 20; $i++) {
            if($i%3==0 || $i%4==0){
                DB::table('tables')->insert([
                    'status' => "free",
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }else{
                DB::table('tables')->insert([
                    'status' => "busy",
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
            
        }
    }
}
