<?php

namespace Database\Seeders;


use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 2 manager users
        for ($i = 1; $i <= 2; $i++) {
            DB::table('users')->insert([
                'name' => "Manager $i",
                'email' => "manager$i@gmail.com",
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'type' => 'manager',
                'remember_token' => Str::random(10),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

        // Create 5 chef users
        for ($i = 1; $i <= 5; $i++) {
            DB::table('users')->insert([
                'name' => "Chef $i",
                'email' => "chef$i@gmail.com",
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'type' => 'chef',
                'remember_token' => Str::random(10),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

        // Create 2 cashier users
        for ($i = 1; $i <= 2; $i++) {
            DB::table('users')->insert([
                'name' => "Cashier $i",
                'email' => "cashier$i@gmail.com",
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'type' => 'cashier',
                'remember_token' => Str::random(10),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

        // Create 10 waiter users
        for ($i = 1; $i <= 10; $i++) {
            DB::table('users')->insert([
                'name' => "Waiter $i",
                'email' => "waiter$i@gmail.com",
                'email_verified_at' => now(),
                'password' => Hash::make('password'),
                'type' => 'waiter',
                'remember_token' => Str::random(10),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
