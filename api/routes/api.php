<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WaiterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->group(function () {

    //WaiterController
    Route::get('/products',[WaiterController::class, 'products'])->middleware('role:waiter');
    Route::get('/tables',[WaiterController::class, 'tables'])->middleware('role:waiter');
    Route::post('/order',[WaiterController::class, 'order'])->middleware('role:waiter');
    Route::get('/myorders',[WaiterController::class, 'myorders'])->middleware('role:waiter');

});
    