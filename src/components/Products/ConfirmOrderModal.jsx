import * as React from "react";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";

import OrdersDataTable from "./OrdersDataTable";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export default function ConfirmOrderModal({
  open,
  orderDetails,
  onClose,
  onSaveChanges,
  onTableIdChange,
  tableId,
  errorTableIdValue,
}) {
  return (
    <React.Fragment>
      <BootstrapDialog
        onClose={onClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          Send Orders
        </DialogTitle>
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent dividers>
          <TextField
            fullWidth
            error={!!errorTableIdValue}
            helperText={errorTableIdValue}
            label="Table id"
            onChange={(e) => onTableIdChange(e)}
            value={tableId}
            placeholder="6..."
            sx={{ mb: 1 }}
            required
          />
          <OrdersDataTable orderDetails={orderDetails} />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Close</Button>
          <Button onClick={onSaveChanges}>Send</Button>
        </DialogActions>
      </BootstrapDialog>
    </React.Fragment>
  );
}
