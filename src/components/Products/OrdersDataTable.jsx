import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

export default function BasicTable({ orderDetails }) {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 400 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="left">№</TableCell>
            <TableCell align="left">Name</TableCell>
            <TableCell align="left">Quantity</TableCell>
            <TableCell align="left">Additional</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orderDetails.map((order, index) => (
            <TableRow
              key={index}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left">{index + 1}</TableCell>
              <TableCell align="left">{order.product_name}</TableCell>
              <TableCell align="left">{order.quantity}</TableCell>
              <TableCell align="left">{order.additional}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
