import { useState } from "react";

import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea, CardActions } from "@mui/material";
import { green, red, yellow } from "@mui/material/colors";

import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import ShoppingBagIcon from "@mui/icons-material/ShoppingBag";
import CreateIcon from "@mui/icons-material/Create";

import AdditionalModal from "./AdditionalModal";

export default function ProductCard({
  product,
  orderDetails,
  SetOrderDetails,
  onOrderClick,
  onIncrement,
  onDecrement,
}) {
  const cGreen = green["A400"];
  const cRed = red["A400"];
  const cYellow = yellow["600"];

  const existingOrder = orderDetails.find(
    (order) => order.product_id === product.id
  );
  const [activeAdditional, setActiveAdditional] = useState(false);
  const [additionalInfo, setAdditionalInfo] = useState(
    existingOrder ? existingOrder.additional : ""
  );

  const handleAdditionalInputChange = (e) => {
    setAdditionalInfo(e.target.value);
  };

  const handleSaveChanges = () => {
    const updatedOrderDetails = orderDetails.map((order) =>
      order.product_id === existingOrder.product_id
        ? { ...order, additional: additionalInfo }
        : order
    );

    SetOrderDetails(updatedOrderDetails);
    setActiveAdditional(false);
  };

  const handleOrderClick = () => {
    setAdditionalInfo("");
    onOrderClick(product);
  };

  return (
    <Card sx={{ maxWidth: 345, mb: 2, ml: 1 }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="200"
          image={`${import.meta.env.VITE_API_URL}/storage/${product.photo}`}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h6" component="div">
            {product.name}{" "}
            {existingOrder ? " X " + existingOrder.quantity : null}
          </Typography>
          <Typography
            variant="button"
            display="block"
            sx={{
              position: "absolute",
              top: 0,
              right: 0,
              backgroundColor: "rgba(142, 142, 142, 0.45)",
              padding: "0.5rem",
              borderRadius: "0 0 0 5px",
            }}
          >
            {product.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}
          </Typography>
          <Typography
            sx={{
              display: "-webkit-box",
              overflow: "hidden",
              WebkitBoxOrient: "vertical",
              WebkitLineClamp: 2,
            }}
            variant="body2"
            color="text.secondary"
          >
            {product.description}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        {!existingOrder && (
          <>
            <Button
              onClick={handleOrderClick}
              size="small"
              color="primary"
              variant="contained"
            >
              <ShoppingBagIcon />
              Order
            </Button>
          </>
        )}
        {!!existingOrder && (
          <>
            <Button
              onClick={() => onIncrement(existingOrder)}
              size="small"
              style={{ backgroundColor: cGreen }}
              variant="contained"
            >
              <AddIcon />
            </Button>
            <Button
              onClick={() => onDecrement(existingOrder)}
              size="small"
              style={{ backgroundColor: cRed }}
              variant="contained"
            >
              <RemoveIcon />
            </Button>
            <Button
              onClick={() => setActiveAdditional(true)}
              size="small"
              style={{ backgroundColor: cYellow }}
              variant="contained"
            >
              <CreateIcon />
            </Button>
            <AdditionalModal
              open={activeAdditional}
              handleClose={() => setActiveAdditional(false)}
              onSaveChanges={handleSaveChanges}
              onAdditionalInputChange={handleAdditionalInputChange}
              additionalInfo={additionalInfo}
            />
          </>
        )}
      </CardActions>
    </Card>
  );
}
