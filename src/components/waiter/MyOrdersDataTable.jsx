import * as React from "react";
import {
  DataGrid,
  GridToolbar,
  gridPageCountSelector,
  gridPageSelector,
  useGridApiContext,
  useGridSelector,
} from "@mui/x-data-grid";
import Pagination from "@mui/material/Pagination";
import PaginationItem from "@mui/material/PaginationItem";

const CustomChefCell = ({ value }) => {
  let chefName = value === null ? "Not accepted" : value;
  return (
    <div style={{ color: value === null ? "#ffd000" : null }}>{chefName}</div>
  );
};
const CustomStatusCell = ({ value }) => {
  let cellStyle = {
    borderRadius: "13px",
    padding: "1px 5px",
    border: "1.1px solid white",
  };

  let statusText = value;
  switch (value) {
    case "ordered":
      statusText = "Ordered";
      cellStyle.color = "#ffd000";
      cellStyle.borderColor = "#ffd000";
      break;
    case "acceptedByChef":
      statusText = "Accepted by Chef";
      cellStyle.color = "#0010ff";
      cellStyle.borderColor = "#0010ff";
      break;
    case "readyToDelivery":
      statusText = "Ready to Delivery";
      cellStyle.color = "#00d8ff";
      cellStyle.borderColor = "#00d8ff";
      break;
    case "acceptedByClient":
      statusText = "Accepted by Client";
      cellStyle.color = "#00ffc8";
      cellStyle.borderColor = "#00ffc8";
      break;
    case "paid":
      statusText = "Paid";
      cellStyle.color = "#00e676";
      cellStyle.borderColor = "#00e676";
      break;
    case "rejected":
      statusText = "Rejected";
      cellStyle.color = "#ff1744";
      cellStyle.borderColor = "#ff1744";
      break;
    default:
      break;
  }

  return <div style={cellStyle}>{statusText}</div>;
};

const CustomTimeCell = ({ value }) => {
  const date = new Date(value);

  const formattedTime = date.toLocaleString("ru-Ru", {
    hour: "numeric",
    minute: "numeric",
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour12: false,
  });

  return <div>{formattedTime}</div>;
};

function CustomPagination() {
  const apiRef = useGridApiContext();
  const page = useGridSelector(apiRef, gridPageSelector);
  const pageCount = useGridSelector(apiRef, gridPageCountSelector);

  return (
    <Pagination
      color="primary"
      variant="outlined"
      shape="rounded"
      page={page + 1}
      count={pageCount}
      // @ts-expect-error
      renderItem={(props2) => <PaginationItem {...props2} disableRipple />}
      onChange={(event, value) => apiRef.current.setPage(value - 1)}
    />
  );
}

const PAGE_SIZE = 10;

export default function ColumnSelectorGrid({ orders }) {
  const myData = {
    columns: [
      {
        field: "id",
        hide: true,
      },
      {
        field: "chef_name",
        headerName: "Chef",
        width: 110,
        renderCell: (params) => <CustomChefCell {...params} />,
      },
      {
        field: "status",
        headerName: "Status",
        type: "singleSelect",
        valueOptions: [
          { value: "ordered", label: "Ordered" },
          { value: "acceptedByChef", label: "Accepted by Chef" },
          { value: "readyToDelivery", label: "Ready to Delivery" },
          { value: "acceptedByClient", label: "Accepted by Client" },
          { value: "paid", label: "Paid" },
          { value: "rejected", label: "Rejected" },
        ],
        width: 150,
        renderCell: (params) => <CustomStatusCell {...params} />,
      },
      {
        field: "price",
        headerName: "Price",
        type: "number",
        width: 140,
        editable: false,
      },
      {
        field: "table_id",
        headerName: "Table",
        type: "number",
        width: 140,
      },
      {
        field: "updated_at",
        headerName: "Time",
        width: 200,
        renderCell: (params) => <CustomTimeCell {...params} />,
      },
    ],
    experimentalFeatures: {
      ariaV7: true,
    },
    initialState: {
      columns: {
        columnVisibilityModel: {
          id: false,
        },
      },
    },
    rows: orders,
  };

  const [paginationModel, setPaginationModel] = React.useState({
    pageSize: PAGE_SIZE,
    page: 0,
  });

  return (
    <>
      <DataGrid
        checkboxSelection
        paginationModel={paginationModel}
        onPaginationModelChange={setPaginationModel}
        pageSizeOptions={[PAGE_SIZE]}
        {...myData}
        slots={{
          toolbar: GridToolbar,
          pagination: CustomPagination,
        }}
      />
    </>
  );
}
