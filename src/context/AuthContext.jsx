import { createContext, useContext, useEffect, useState } from "react";
import axios from "../api/axios";
import { useNavigate } from "react-router-dom";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [errors, setErrors] = useState([]);
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();

  const csrf = () => axios.get("/sanctum/csrf-cookie");

  const saveLoginSessionStorage = () => {
    sessionStorage.setItem("loggedIn", true);
  };

  const getUser = async () => {
    try {
      const { data } = await axios.get("/api/user");
      setUser(data);
      // console.log(data);
    } catch (e) {
      console.log(e);
    }
  };

  const login = async ({ ...data }) => {
    await csrf();
    setErrors([]);
    try {
      await axios.post("/login", data);
      await getUser();
      saveLoginSessionStorage();
      navigate("/");
    } catch (e) {
      console.log(e);
      if (e.response.status === 422) {
        setErrors(e.response.data.errors);
      }
    }
  };

  const register = async ({ ...data }) => {
    await csrf();
    setErrors([]);
    try {
      await axios.post("/register", data);
      await getUser();
      saveLoginSessionStorage();
      navigate("/");
    } catch (e) {
      if (e.response.status === 422) {
        setErrors(e.response.data.errors);
      }
    }
  };

  const logout = async () => {
    await axios.post("/logout").then(() => {
      setUser(null);
      sessionStorage.setItem("loggedIn", false);
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const isLoggedIn = sessionStorage.getItem("loggedIn") === "true";

        if (isLoggedIn && !user) {
          await getUser();
        }
      } catch (error) {
        console.error("Error fetching user data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [user]);

  return (
    <AuthContext.Provider
      value={{ user, errors, getUser, login, register, logout }}
    >
      {!loading && children}
    </AuthContext.Provider>
  );
};

export default function useAuthContext() {
  return useContext(AuthContext);
}
