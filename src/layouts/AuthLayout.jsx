import { Navigate, Outlet } from "react-router-dom";
import useAuthContext from "../context/AuthContext";
import Nav from "../components/Nav";

const AuthLayout = () => {
  const { user } = useAuthContext();

  return user ? (
    <>
      <Nav />
      <Outlet />
    </>
  ) : (
    <Navigate to="/login" />
  );
};

export default AuthLayout;
