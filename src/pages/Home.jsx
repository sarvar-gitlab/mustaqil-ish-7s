import { Helmet } from "react-helmet";
import useAuthContext from "../context/AuthContext";

import * as React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Unstable_Grid2";

import BarChart from "../components/home/BarChart";
import PieActiveArc from "../components/home/PieActiveArc";
import BasicLineChart from "../components/home/BasicLineChart";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const Home = () => {
  const { user } = useAuthContext();

  return (
    <>
      <Helmet>
        <title>Dashboard - RestOne</title>
      </Helmet>
      <Box sx={{ flexGrow: 1, mx: 1, mt: 2, mb: 3 }}>
        <Grid container spacing={2}>
          <Grid xs={12} md={6}>
            <Item>
              <BarChart />
            </Item>
          </Grid>
          <Grid xs={12} md={6}>
            <Item>
              <BasicLineChart />
              <PieActiveArc />
            </Item>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default Home;
