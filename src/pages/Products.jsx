import { Helmet } from "react-helmet";
import ProductCard from "../components/Products/ProductCard";
import axios from "../api/axios";
import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import useAuthContext from "../context/AuthContext";

import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import Fab from "@mui/material/Fab";
import SendIcon from "@mui/icons-material/Send";
import Grid from "@mui/material/Grid";

import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";

import ConfirmOrderModal from "../components/Products/ConfirmOrderModal";

const style = {
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "space-around",
  marginTop: "20px",
};

export default function Products() {
  const { user } = useAuthContext();
  const [productData, setProductData] = useState(null);
  const [orderDetails, SetOrderDetails] = useState([]);
  const [searchParams, setSearchParams] = useSearchParams();
  const search = searchParams.get("search") || "";
  const [currentPage, setCurrentPage] = useState(
    parseInt(searchParams.get("currentPage")) || 1
  );
  const [countPages, setCountPages] = useState();
  const [sendClicked, setSendClicked] = useState(false);
  const [tables, setTables] = useState();
  const [tableId, setTableId] = useState("");
  const [errorTableIdValue, setErrorTableIdValue] = useState();

  const getProducts = async () => {
    const { data } = await axios.get(
      `/api/products?search=${search}&page=${currentPage}`
    );
    setProductData(data.products.data);
    setCountPages(data.products.last_page);
  };

  const getTables = async () => {
    try {
      const response = await axios.get(`/api/tables`);
      setTables(response.data.tables);
    } catch (error) {
      console.log(error);
    }
  };

  const handleSearch = (e) => {
    setSearchParams({ search: e.target.value, currentPage: 1 });
    setCurrentPage(1);
  };

  const handleIncrement = (clickedOrder) => {
    const updatedOrders = orderDetails.map((order) => {
      if (order === clickedOrder) {
        return { ...order, quantity: order.quantity + 1 };
      }
      return order;
    });

    SetOrderDetails(updatedOrders);
  };

  const handleDecrement = (clickedOrder) => {
    const updatedOrders = orderDetails.map((order) => {
      if (order === clickedOrder) {
        const updatedCount = order.quantity - 1;
        if (updatedCount <= 0) {
          return null;
        }
        return { ...order, quantity: updatedCount };
      }
      return order;
    });

    const filteredOrders = updatedOrders.filter((order) => order !== null);

    SetOrderDetails(filteredOrders);
  };

  const handleClickedConfirmationOrder = () => {
    setSendClicked(true);
    getTables();
  };

  const handleSendOrders = async () => {
    const selectedTableId = parseInt(tableId);

    if (selectedTableId <= tables.length) {
      setErrorTableIdValue(null);
      try {
        const { data } = await axios.post(`/api/order`, {
          table_id: selectedTableId,
          waiter_id: user.id,
          order_details: orderDetails,
        });
        if (data.status === "success") {
          SetOrderDetails([]);
          console.log("Orders success Sended!", data);
        } else {
          console.log("something is wrong");
        }

        setSendClicked(false);
      } catch (error) {
        setErrorTableIdValue("something is wrong");
        console.log(error);
      }
    } else {
      setErrorTableIdValue("Please enter valid Table id");
    }
  };

  const handlePageChange = (event, newPage) => {
    setCurrentPage(newPage);
    setSearchParams({ currentPage: newPage });
    window.scrollTo(0, 0);
  };

  useEffect(() => {
    getProducts();
  }, [search, currentPage]);
  return (
    <>
      <Helmet>
        <title>Products - RestOne</title>
      </Helmet>
      <div style={style}>
        <Typography variant="h5" gutterBottom>
          Products
        </Typography>
        <TextField
          onChange={handleSearch}
          label="Search"
          variant="outlined"
          value={search}
          size="small"
        />
      </div>

      <div style={style}>
        {!!productData &&
          productData.map((product, index) => (
            <ProductCard
              key={index}
              product={product}
              orderDetails={orderDetails}
              SetOrderDetails={SetOrderDetails}
              onOrderClick={(clickedProduct) => {
                const existingOrder = orderDetails.find(
                  (order) => order.product_id === clickedProduct.id
                );

                if (!existingOrder) {
                  SetOrderDetails([
                    ...orderDetails,
                    {
                      product_id: clickedProduct.id,
                      quantity: 1,
                      product_name: clickedProduct.name,
                    },
                  ]);
                } else {
                  const updatedOrders = orderDetails.filter(
                    (order) => !(order.product_id === clickedProduct.id)
                  );
                  SetOrderDetails(updatedOrders);
                }
              }}
              onIncrement={handleIncrement}
              onDecrement={handleDecrement}
            />
          ))}
      </div>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="flex-start"
      >
        <Stack spacing={2}>
          <Pagination
            count={countPages}
            color="primary"
            page={currentPage}
            onChange={handlePageChange}
          />
        </Stack>
      </Grid>

      <Grid
        container
        direction="row"
        justifyContent="flex-end"
        alignItems="flex-start"
      >
        {orderDetails.length > 0 && (
          <>
            <Fab
              sx={{ mr: 1, mb: 1 }}
              color="primary"
              aria-label="send-orders"
              onClick={handleClickedConfirmationOrder}
            >
              <SendIcon />
            </Fab>
            <ConfirmOrderModal
              open={sendClicked}
              orderDetails={orderDetails}
              onClose={() => setSendClicked(false)}
              onSaveChanges={handleSendOrders}
              onTableIdChange={(e) => setTableId(e.target.value)}
              tableId={tableId}
              errorTableIdValue={errorTableIdValue}
            />
          </>
        )}
      </Grid>
    </>
  );
}
