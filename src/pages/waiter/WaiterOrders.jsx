import React, { useEffect, useState } from "react";
import axios from "./../../api/axios";
import { Helmet } from "react-helmet";

import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Unstable_Grid2";

import MyOrdersDataTable from "../../components/waiter/MyOrdersDataTable";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function WaiterOrders() {
  const [myOrders, setMyOrders] = useState([]);

  const getOrders = async () => {
    try {
      const { data } = await axios.get(`/api/myorders`);
      setMyOrders(data.myorders);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getOrders();
  }, []);

  return (
    <>
      <Helmet>
        <title>My orders - RestOne</title>
      </Helmet>

      <Box sx={{ flexGrow: 1, mx: 1, mt: 2 }}>
        <Grid container justifyContent="center">
          <Grid md={12} lg={6}>
            <Item>
              <MyOrdersDataTable orders={myOrders} />
            </Item>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
